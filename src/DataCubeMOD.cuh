extern "C"{
	#include <stdlib.h>
	#include <stdio.h>
	#include <string.h>
	#include <math.h>
	#include <stdint.h>
	#include <limits.h>

	#include "WCS.h"
	#include "DataCube.h"
	#include "Source.h"
	#include "statistics_flt.h"
	#include "statistics_dbl.h"


	// ----------------------------------------------------------------- //
	// Declaration of private properties and methods of class DataCube   //
	// ----------------------------------------------------------------- //

	CLASS DataCube
	{
		char   *data;
		size_t  data_size;
		Header *header;
		int     data_type;
		int     word_size;
		size_t  dimension;
		size_t  axis_size[4];
		double  bscale;
		double  bzero;
		bool    verbosity;
	};
}
#include "cp_select.h"
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/iterator/transform_iterator.h>

#include <thrust/host_vector.h>

// Parallel functions for CUDA
__global__ void DataCube_set_masked_8_device(char* cube, char* maskCube, const double value, size_t sizeX, size_t sizeY, size_t sizeZ);
__global__ void DataCube_set_masked_8_device_with_copy(char* cube, char* maskCube, char* sourceCube, const double value, size_t sizeX, size_t sizeY, size_t sizeZ);

__global__ void DataCube_copy_blanked_device(char* dest, char* source, size_t sizeX, size_t sizeY, size_t sizeZ);
__global__ void DataCube_mask_8_device(char* cube, char* maskCube, const double threshold, const uint8_t value, size_t sizeX, size_t sizeY, size_t sizeZ);
__global__ void DataCube_mask_8_device_blanks(char* cube, char* maskCube, char* blanksCube, const double threshold, const uint8_t value, size_t sizeX, size_t sizeY, size_t sizeZ);

PUBLIC DataCube *DataCube_copy_noData(const DataCube *source);

PUBLIC void DataCube_run_scfind_cuda(const DataCube *self, DataCube *maskCube, const Array_dbl *kernels_spat, const Array_siz *kernels_spec, const double threshold, const double maskScaleXY, const noise_stat method, const int range, const int scaleNoise, const size_t snWindowXY, const size_t snWindowZ, const size_t snGridXY, const size_t snGridZ, const bool snInterpol, const time_t start_time, const clock_t start_clock);

PUBLIC void DataCube_gaussian_filter_device(char *cube, const double sigma, size_t sizeX, size_t sizeY, size_t sizeZ, int data_type);
PUBLIC void DataCube_gaussian_filter_device_sharedMem(char *cube, const double sigma, size_t sizeX, size_t sizeY, size_t sizeZ, int data_type);
__global__ void Xfilters_flt(char *data, char *data_prev, size_t radius, size_t sizeX, size_t sizeY, size_t sizeZ);
__global__ void Yfilters_flt(char *data, char *data_prev, size_t radius, size_t sizeX, size_t sizeY, size_t sizeZ);

PUBLIC void DataCube_boxcar_filter_device(char *cube, size_t radius, size_t sizeX, size_t sizeY, size_t sizeZ, int data_type);
PUBLIC void DataCube_boxcar_filter_device_sharedMem(char *cube, size_t radius, size_t sizeX, size_t sizeY, size_t sizeZ, int data_type);
PUBLIC void DataCube_boxcar_filter_device_localMem(char *cube, size_t radius, size_t sizeX, size_t sizeY, size_t sizeZ, int data_type);
__global__ void Zfilters_flt(char *data, char *data_prev, size_t radius, size_t sizeX, size_t sizeY, size_t sizeZ);

__device__ void filter_boxcar_1d_flt_device(float *data, float *dataCpyArray, const size_t size, const size_t filter_radius, size_t stride);
