#include "DataCubeMOD.cuh"

// ----------------------------------------------------------------- //
// Set masked pixels to constant value                               //
// ----------------------------------------------------------------- //
// Arguments:                                                        //
//                                                                   //
// Return value:                                                     //
//                                                                   //
//   No return value.                                                //
//                                                                   //
// Description:                                                      //
//                                                                   //
//   Public method for replacing the values of all pixels in the     //
//   data cube that are non-zero in the mask cube to their signum    //
//   multiplied by the specified value.                              //
// ----------------------------------------------------------------- //

__global__ void DataCube_set_masked_8_device(char* cube, char* maskCube, const double value, size_t sizeX, size_t sizeY, size_t sizeZ)
{
	int col   = threadIdx.x + blockIdx.x * blockDim.x;
	int row   = threadIdx.y + blockIdx.y * blockDim.y;
	int depth = threadIdx.z + blockIdx.z * blockDim.z;

	if ((col < sizeX) && (row < sizeY) && (depth < sizeZ)){
		uint8_t *dataMask_i = (uint8_t*) maskCube + col + row * sizeX + depth * sizeX * sizeY;
		float *data_i = (float*) cube + col + row * sizeX + depth * sizeX * sizeY;
		if (*dataMask_i) {
			*data_i = copysignf(value, *data_i);
		}else if(isnan(*data_i)) {
			*data_i = 0.0;
		}
	}
	return;
}

//DataCube_set_masked_8_device + copy device->device
__global__ void DataCube_set_masked_8_device_with_copy(char* cube, char* maskCube, char* sourceCube, const double value, size_t sizeX, size_t sizeY, size_t sizeZ)
{
	int col   = threadIdx.x + blockIdx.x * blockDim.x;
	int row   = threadIdx.y + blockIdx.y * blockDim.y;
	int depth = threadIdx.z + blockIdx.z * blockDim.z;

	if ((col < sizeX) && (row < sizeY) && (depth < sizeZ)){
		uint8_t *dataMask_i = (uint8_t*) maskCube + col + row * sizeX + depth * sizeX * sizeY;
		float dataSource_i = *((float*) sourceCube + col + row * sizeX + depth * sizeX * sizeY);
		float *data_i = (float*) cube + col + row * sizeX + depth * sizeX * sizeY;

		if (*dataMask_i) {
			*data_i = copysignf(value, dataSource_i);
		}else{
			if(isnan(dataSource_i)) {
				*data_i = 0.0;
			}else{
				*data_i = dataSource_i;
			}
		}
	}
	return;
}


// ----------------------------------------------------------------- //
// Copy blanked pixels from one cube to another                      //
// ----------------------------------------------------------------- //
// Arguments:                                                        //
//                                                                   //
// Return value:                                                     //
//                                                                   //
//   No return value.                                                //
//                                                                   //
// Description:                                                      //
//                                                                   //
//   Public method for copying blanked pixels from one cube to the   //
//   other. Both cubes need to be of the same size in x, y and z and //
//   must be of floating-point type. Blanked pixels are assumed to   //
//   be represented by NaN (not a number).                           //
// ----------------------------------------------------------------- //

__global__ void DataCube_copy_blanked_device(char* dest, char* source, size_t sizeX, size_t sizeY, size_t sizeZ)
{
	int col   = threadIdx.x + blockIdx.x * blockDim.x;
	int row   = threadIdx.y + blockIdx.y * blockDim.y;
	int depth = threadIdx.z + blockIdx.z * blockDim.z;

	if ((col < sizeX) && (row < sizeY) && (depth < sizeZ)){
			float *source_i = (float*) source + col + row * sizeX + depth * sizeX * sizeY;
			float *dest_i = (float*) dest + col + row * sizeX + depth * sizeX * sizeY;

			if(isnan(*source_i)) *dest_i = NAN;
	}
	return;
}


// ----------------------------------------------------------------- //
// Mask pixels of abs(value) > threshold                             //
// ----------------------------------------------------------------- //
// Arguments:                                                        //
//                                                                   //
// Return value:                                                     //
//                                                                   //
//   No return value.                                                //
//                                                                   //
// Description:                                                      //
//                                                                   //
//   Public method for setting pixels in the mask cube to 1 when     //
//   their absolute value in the data cube is greater than the spe-  //
//   cified threshold.                                               //
// ----------------------------------------------------------------- //

__global__ void DataCube_mask_8_device(char* cube, char* maskCube, const double threshold, const uint8_t value, size_t sizeX, size_t sizeY, size_t sizeZ)
{
	int col   = threadIdx.x + blockIdx.x * blockDim.x;
	int row   = threadIdx.y + blockIdx.y * blockDim.y;
	int depth = threadIdx.z + blockIdx.z * blockDim.z;

	if ((col < sizeX) && (row < sizeY) && (depth < sizeZ)){
			uint8_t *dataMask_i = (uint8_t*) maskCube + col + row * sizeX + depth * sizeX * sizeY;
			float *data_i = (float*) cube + col + row * sizeX + depth * sizeX * sizeY;
			const float  thresh_n = -threshold;
			const float  thresh_p = threshold;

			if(*data_i > thresh_p || *data_i < thresh_n) *dataMask_i = value;
	}
	return;
}

//DataCube_mask_8_device + DataCube_copy_blanked_device
__global__ void DataCube_mask_8_device_blanks(char* cube, char* maskCube, char* blanksCube, const double threshold, const uint8_t value, size_t sizeX, size_t sizeY, size_t sizeZ)
{
	int col   = threadIdx.x + blockIdx.x * blockDim.x;
	int row   = threadIdx.y + blockIdx.y * blockDim.y;
	int depth = threadIdx.z + blockIdx.z * blockDim.z;

	if ((col < sizeX) && (row < sizeY) && (depth < sizeZ)){
			uint8_t *dataMask_i = (uint8_t*) maskCube + col + row * sizeX + depth * sizeX * sizeY;
			float *dataBlanks_i = (float*) blanksCube + col + row * sizeX + depth * sizeX * sizeY;
			float *data_i = (float*) cube + col + row * sizeX + depth * sizeX * sizeY;
			const float  thresh_n = -threshold;
			const float  thresh_p = threshold;

			if (!isnan(*dataBlanks_i) && (*data_i > thresh_p || *data_i < thresh_n)) *dataMask_i = value;
			//if (!isnan(*dataBlanks_i) && (*data_i > thresh_p)) *dataMask_i = value;
	}
	return;
}


// ----------------------------------------------------------------- //
// Algorithms for median calculation on the device                   //
// ----------------------------------------------------------------- //
// Arguments:                                                        //
//                                                                   //
// Return value:                                                     //
//                                                                   //
//   No return value.                                                //
//                                                                   //
// Description:                                                      //
//                                                                   //
// ----------------------------------------------------------------- //
__global__ void DataCube_copy_abs(char* dest, char* source, size_t sizeX, size_t sizeY, size_t sizeZ)
{
	int col   = threadIdx.x + blockIdx.x * blockDim.x;
	int row   = threadIdx.y + blockIdx.y * blockDim.y;
	int depth = threadIdx.z + blockIdx.z * blockDim.z;

	if ((col < sizeX) && (row < sizeY) && (depth < sizeZ)){
			float *source_i = (float*) source + col + row * sizeX + depth * sizeX * sizeY;
			float *dest_i = (float*) dest + col + row * sizeX + depth * sizeX * sizeY;

			*dest_i = fabsf(*source_i);
	}
	return;
}

// note: functor inherits from unary_function
struct absolute_value : public thrust::unary_function<float,float>
{
  __host__ __device__
  float operator()(float x) const
  {
    return fabsf(x);
  }
};

struct absolute_value_nan_check : public thrust::unary_function<float,float>
{
  __host__ __device__
  float operator()(float x) const
  {
	  if (isnan(x)){
		return 0.0;
	  }else{
		return fabsf(x);
	  }
  }
};

float Compute_median_by_sorting(thrust::device_ptr<float> dev_ptr, size_t size){
	thrust::sort(dev_ptr, dev_ptr + size);
	cudaDeviceSynchronize();

	return dev_ptr[(size+1)/2 -1];
}


float mad_val_flt_new(const float *data, const size_t size, const float value, const size_t cadence, const int range)
{
	// Create copy of data array with specified range and cadence
	const size_t data_copy_size = (range == 0) ? (size / cadence) : (size / (2 * cadence));
	float *data_copy = (float *)memory(MALLOC, data_copy_size, sizeof(float));
	
	// Some settings
	const float *ptr = data + size; //Puntero a la siguiente posicion a la ultima en los datos
	float *ptr_copy = data_copy;    //Puntero al array copia
	size_t counter = 0;
	
	// Copy |*ptr - value| into array copy
	while((ptr -= cadence) > data && counter < data_copy_size) 
	{
		if((range < 0 && *ptr < 0.0) || (range == 0 && IS_NOT_NAN(*ptr)) || (range > 0 && *ptr > 0.0))
		{ 
			ptr_copy[counter] = *ptr - value;
			++counter;
		}
	}
	//Basicamente, rellena el cubo copia con los datos que coincidan con el signo de range (todos si es 0), en orden inverso y saltandose una parte (cadence marca el paso)
	//Hasta un máximo de datos no muy superable


	// Determine median
	const float result = median_flt(data_copy, counter, false);
	
	// Clean up
	free(data_copy);
	
	return result;
}

PUBLIC double DataCube_stat_mad_new(const DataCube *self, const double value, const size_t cadence, const int range)
{
	// Sanity checks
	check_null(self);
	check_null(self->data);
	ensure(self->data_type == -32 || self->data_type == -64, "Cannot evaluate MAD for integer array.");
	
	// Derive MAD of data copy
	if(self->data_type == -32) return mad_val_flt_new((float *)self->data, self->data_size, value, cadence ? cadence : 1, range);
	return mad_val_dbl((double *)self->data, self->data_size, value, cadence ? cadence : 1, range);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ----------------------------------------------------------------- //
// Run Smooth + Clip (S+C) finder on data cube                       //
// ----------------------------------------------------------------- //
// Arguments:                                                        //
//                                                                   //
//   (1) self         - Data cube to run the S+C finder on.          //
//   (2) maskCube     - Mask cube for recording detected pixels.     //
//   (3) kernels_spat - List of spatial smoothing lengths correspon- //
//                      ding to the FWHM of the Gaussian kernels to  //
//                      be applied; 0 = no smoothing.                //
//   (4) kernels_spec - List of spectral smoothing lengths corre-    //
//                      sponding to the widths of the boxcar filters //
//                      to be applied. Must be odd or 0.             //
//   (5) threshold    - Relative flux threshold to be applied.       //
//   (6) maskScaleXY  - Already detected pixels will be set to this  //
//                      value times the original rms of the data be- //
//                      fore smoothing the data again.               //
//   (7) method        - Method to use for measuring the noise in    //
//                      the smoothed copies of the cube; can be      //
//                      NOISE_STAT_STD, NOISE_STAT_MAD or            //
//                      NOISE_STAT_GAUSS for standard deviation,     //
//                      median absolute deviation and Gaussian fit   //
//                      to flux histogram, respectively.             //
//   (8) range        - Flux range to used in noise measurement, Can //
//                      be -1, 0 or 1 for negative only, all or po-  //
//                      sitive only.                                 //
//   (9) scaleNoise   - 0 = no noise scaling; 1 = global noise sca-  //
//                      ling; 2 = local noise scaling. Applied after //
//                      each smoothing operation.                    //
//  (10) snWindowXY   - Spatial window size for local noise scaling. //
//                      See DataCube_scale_noise_local() for de-     //
//                      tails.                                       //
//  (11) snWindowZ    - Spectral window size for local noise sca-    //
//                      ling. See DataCube_scale_noise_local() for   //
//                      details.                                     //
//  (12) snGridXY     - Spatial grid size for local noise scaling.   //
//                      See DataCube_scale_noise_local() for de-     //
//                      tails.                                       //
//  (13) snGridZ      - Spectral grid size for local noise scaling.  //
//                      See DataCube_scale_noise_local() for de-     //
//                      tails.                                       //
//  (14) snInterpol   - Enable interpolation for local noise scaling //
//                      if true. See DataCube_scale_noise_local()    //
//                      for details.                                 //
//  (15) start_time   - Arbitrary time stamp; progress time of the   //
//                      algorithm will be calculated and printed re- //
//                      lative to start_time.                        //
//  (16) start_clock  - Arbitrary clock count; progress time of the  //
//                      algorithm in term of CPU time will be calcu- //
//                      lated and printed relative to clock_time.    //
//                                                                   //
// Return value:                                                     //
//                                                                   //
//   No return value.                                                //
//                                                                   //
// Description:                                                      //
//                                                                   //
//   Public method for running the Smooth + Clip (S+C) finder on the //
//   specified data cube. The S+C finder will smooth the data on the //
//   specified spatial and spectral scales, applying a Gaussian fil- //
//   ter in the spatial domain and a boxcar filter in the spectral   //
//   domain. It will then measure the noise level in each iteration  //
//   and mark all pixels with absolute values greater than or equal  //
//   to the specified threshold (relative to the noise level) as 1   //
//   in the specified mask cube, which must be of 8-bit integer      //
//   type, while non-detected pixels will be set to a value of 0.    //
//   Pixels already detected in a previous iteration will be set to  //
//   maskScaleXY times the original rms noise level of the data be-  //
//   fore smoothing.                                                 //
//   The input data cube must be a 32 or 64 bit floating point data  //
//   array. The spatial kernel sizes must be positive floating point //
//   values that represent the FWHM of the Gaussian kernels to be    //
//   applied in the spatial domain. The spectral kernel sizes must   //
//   be positive, odd integer numbers representing the widths of the //
//   boxcar filters to be applied in the spectral domain. The thre-  //
//   shold is relative to the noise level and should be a floating   //
//   point number greater than about 3.0. Lastly, the value of       //
//   maskScaleXY times the original rms of the data will be used to  //
//   replace pixels in the data cube that were already detected in a //
//   previous iteration. This is to ensure that any sources in the   //
//   data will not be smeared out beyond the extent of the source    //
//   when convolving with large kernel sizes.                        //
//   Several methods are available for measuring the noise in the    //
//   data cube, including the standard deviation, median absolute    //
//   deviation and a Gaussian fit to the flux histogram. These dif-  //
//   fer in their speed and robustness. In addition, the flux range  //
//   used in the noise measurement can be restricted to negative or  //
//   positive pixels only to reduce the impact or actual emission or //
//   absorption featured on the noise measurement.                   //
// ----------------------------------------------------------------- //

PUBLIC void DataCube_run_scfind_cuda(const DataCube *self, DataCube *maskCube, const Array_dbl *kernels_spat, const Array_siz *kernels_spec, const double threshold, const double maskScaleXY, const noise_stat method, const int range, const int scaleNoise, const size_t snWindowXY, const size_t snWindowZ, const size_t snGridXY, const size_t snGridZ, const bool snInterpol, const time_t start_time, const clock_t start_clock)
{
	// Sanity checks
	check_null(self);
	check_null(self->data);
	ensure(self->data_type < 0, "The S+C finder can only be applied to floating-point data.");
	check_null(maskCube);
	check_null(maskCube->data);
	ensure(maskCube->data_type == 8, "Mask cube must be of 8-bit integer type.");
	ensure(self->axis_size[0] == maskCube->axis_size[0] && self->axis_size[1] == maskCube->axis_size[1] && self->axis_size[2] == maskCube->axis_size[2], "Data cube and mask cube have different sizes.");
	check_null(kernels_spat);
	check_null(kernels_spec);
	ensure(Array_dbl_get_size(kernels_spat) && Array_siz_get_size(kernels_spec), "Invalid spatial or spectral kernel list encountered.");
	ensure(threshold >= 0.0, "Negative flux threshold encountered.");
	ensure(method == NOISE_STAT_STD || method == NOISE_STAT_MAD || method == NOISE_STAT_GAUSS, "Invalid noise measurement method: %d.", method);

	// A few additional settings
	const double FWHM_CONST = 2.0 * sqrt(2.0 * log(2.0));  // Conversion between sigma and FWHM of Gaussian function
	size_t cadence = self->data_size / NOISE_SAMPLE_SIZE;  // Stride for noise calculation
	if(cadence < 2) cadence = 1;
	else if(cadence % self->axis_size[0] == 0) cadence -= 1;    // Ensure stride is not equal to multiple of x-axis size
	cadence = 1;

	message("Using a stride of %zu in noise measurement.\n", cadence);
	
	// Print time: start of the Filter
	timestamp(start_time, start_clock);

	//Checking free memory in the device
	size_t freeMemCUDA, totalMemCuda;
	size_t size = (size_t) (self->data_size * self->word_size * sizeof(char));
	size_t sizeMask = (size_t) (maskCube->data_size * maskCube->word_size * sizeof(char));	

	cudaMemGetInfo(&freeMemCUDA,&totalMemCuda);	
	message("Free bytes of device memory:   %11llu", freeMemCUDA);
	message("Size in bytes of the data Cube:%11llu", 2*size + sizeMask);

	// CUBE CREATION
	char* smoothedCubeDevice = NULL;
	cudaMalloc(&smoothedCubeDevice, size);
	cudaMemcpy(smoothedCubeDevice, self->data, size, cudaMemcpyHostToDevice);
	// FOR MEDIAN CALCULATION
	float* smoothedCubeDeviceFloat = (float*)smoothedCubeDevice;
	thrust::device_ptr<float> dev_ptr_smoothed(smoothedCubeDeviceFloat);
	/////////////////////////

	char* cubeFilter = NULL;
	cudaMalloc(&cubeFilter, size);
	// FOR MEDIAN CALCULATION
	float* cubeFilterFloat = (float*)cubeFilter;
	thrust::device_ptr<float> dev_ptr_filter(cubeFilterFloat);
	/////////////////////////

	char* maskCubeDevice = NULL;
	cudaMalloc(&maskCubeDevice, sizeMask);
	cudaMemcpy(maskCubeDevice, maskCube->data, sizeMask, cudaMemcpyHostToDevice);
	///////////////////////////////////

	// Measure noise in original cube with sampling "cadence"
	double rms;
	double rms_smooth;
	

	// MEDIAN CALCULATION

	//// HOST
	//rms = MAD_TO_STD * DataCube_stat_mad(self, 0.0, cadence, range);

	//// DEVICE
	thrust::device_vector<float> vector_smoothed(thrust::make_transform_iterator(dev_ptr_smoothed, absolute_value_nan_check()), thrust::make_transform_iterator(dev_ptr_smoothed + self->data_size, absolute_value_nan_check()));
	rms = MAD_TO_STD * cp_select::flt_median_min(vector_smoothed.begin(), vector_smoothed.end(), 7, 1, 0);
	///////////////////////////////////

	//Dimensiones de bloque y grid para operaciones que vayan pixel a pixel, en todo el cubo
	dim3 dimBlockAllCube(8, 8, 4);
	int GridXAllCube = (self->axis_size[0] + dimBlockAllCube.x - 1) / dimBlockAllCube.x;
	int GridYAllCube = (self->axis_size[1] + dimBlockAllCube.y - 1) / dimBlockAllCube.y;
	int GridZAllCube = (self->axis_size[2] + dimBlockAllCube.z - 1) / dimBlockAllCube.z;
	dim3 dimGridAllCube(GridXAllCube, GridYAllCube, GridZAllCube);

	// Run S+C finder for all smoothing kernels
	for(size_t i = 0; i < Array_dbl_get_size(kernels_spat); ++i)
	{
		for(size_t j = 0; j < Array_siz_get_size(kernels_spec); ++j)
		{			
			message("Smoothing kernel:  [%.1f] x [%zu]", Array_dbl_get(kernels_spat, i), Array_siz_get(kernels_spec, j));

			// Check if any smoothing requested
			if(Array_dbl_get(kernels_spat, i) || Array_siz_get(kernels_spec, j))
			{
				// SETUP PRE-SMOOTHING
				DataCube_set_masked_8_device_with_copy<<<dimGridAllCube, dimBlockAllCube>>>(cubeFilter, maskCubeDevice, smoothedCubeDevice, maskScaleXY * rms, self->axis_size[0], self->axis_size[1], self->axis_size[2]);
				///////////////////////////////////

				// Spatial and spectral smoothing
				//if(Array_dbl_get(kernels_spat, i) > 0.0)   DataCube_gaussian_filter_device(cubeFilter, Array_dbl_get(kernels_spat, i) / FWHM_CONST, self->axis_size[0], self->axis_size[1], self->axis_size[2], self->data_type);
				//if(Array_siz_get(kernels_spec, j) > 0.0)   DataCube_boxcar_filter_device(cubeFilter, (Array_siz_get(kernels_spec, j) / 2), self->axis_size[0], self->axis_size[1], self->axis_size[2], self->data_type);
				if(Array_dbl_get(kernels_spat, i) > 0.0)   DataCube_gaussian_filter_device_sharedMem(cubeFilter, Array_dbl_get(kernels_spat, i) / FWHM_CONST, self->axis_size[0], self->axis_size[1], self->axis_size[2], self->data_type);
				if(Array_siz_get(kernels_spec, j) > 0.0)   DataCube_boxcar_filter_device_sharedMem(cubeFilter, (Array_siz_get(kernels_spec, j) / 2), self->axis_size[0], self->axis_size[1], self->axis_size[2], self->data_type);
				///////////////////////////////////

				// MEDIAN CALCULATION

				//// HOST
				/*DataCube *smoothedCube = DataCube_copy(self);
				cudaDeviceSynchronize();
				cudaMemcpy(smoothedCube->data, cubeFilter, size, cudaMemcpyDeviceToHost);
				rms_smooth = MAD_TO_STD * DataCube_stat_mad(smoothedCube, 0.0, cadence, range);
				message("Noise level:       %.3e", rms_smooth);
				cudaMemcpy(cubeFilter, smoothedCube->data, size, cudaMemcpyHostToDevice);
				DataCube_delete(smoothedCube);*/

				//// DEVICE
				/// cálculo con sorting
				/*DataCube_copy_abs<<<dimGridAllCube, dimBlockAllCube>>>(cubeMedian, cubeFilter, self->axis_size[0], self->axis_size[1], self->axis_size[2]);
				rms_smooth = MAD_TO_STD * Compute_median_by_sorting(dev_ptr_median, self->data_size);*/

				/// el scfind parece seguir fallando con algunos cubos, pero es un poco mas rapido que el sorting.
				/*DataCube_copy_abs<<<dimGridAllCube, dimBlockAllCube>>>(cubeFilter, cubeFilter, self->axis_size[0], self->axis_size[1], self->axis_size[2]);
				thrust::device_vector<float> vector_filter(dev_ptr_filter, dev_ptr_filter + self->data_size);*/

				/// El transform iterator funciona y mejora la ejecucion el reducir el ancho de banfa del sistema
				thrust::device_vector<float> vector_filter(thrust::make_transform_iterator(dev_ptr_filter, absolute_value()), thrust::make_transform_iterator(dev_ptr_filter + self->data_size, absolute_value()));
				rms_smooth = MAD_TO_STD * cp_select::flt_median_min(vector_filter.begin(), vector_filter.end(), 7, 1, 0);

				message("Noise level:       %.3e", rms_smooth);
				///////////////////////////////////

				// UPDATE MASK CUBE
				ensure(threshold > 0.0, "Threshold must be positive.");
				DataCube_mask_8_device_blanks<<<dimGridAllCube, dimBlockAllCube>>>(cubeFilter, maskCubeDevice, smoothedCubeDevice, threshold * rms_smooth, 1, self->axis_size[0], self->axis_size[1], self->axis_size[2]);
				cudaDeviceSynchronize();
				///////////////////////////////////

			}
			else
			{
				// No smoothing required; apply threshold to original cube
				message("Noise level:       %.3e", rms);
				ensure(threshold > 0.0, "Threshold must be positive.");
				DataCube_mask_8_device<<<dimGridAllCube, dimBlockAllCube>>>(smoothedCubeDevice, maskCubeDevice, threshold * rms, 1, self->axis_size[0], self->axis_size[1], self->axis_size[2]);
			}

			// Print time: End of iteration x
			timestamp(start_time, start_clock);
		}
	}

	cudaMemcpy(maskCube->data, maskCubeDevice, sizeMask, cudaMemcpyDeviceToHost);

	cudaFree(smoothedCubeDevice);
	cudaFree(cubeFilter);
	//cudaFree(cubeMedian);

	cudaFree(maskCubeDevice);
	return;
}

// ----------------------------------------------------------------- //
// Copy constructor with no data copy                                //
// ----------------------------------------------------------------- //
// Arguments:                                                        //
//                                                                   //
//   (1) source - Pointer to DataCube object to be copied.           //
//                                                                   //
// Return value:                                                     //
//                                                                   //
//   Pointer to newly created DataCube object.                       //
//                                                                   //
// Description:                                                      //
//                                                                   //
//   Copy constructor. Will create a new DataCube object that is a   //
//   physical copy of the object pointed to by source. A pointer to  //
//   the newly created object will be returned. Note that the de-    //
//   structor will need to be called explicitly once the object is   //
//   no longer required to release any memory allocated to the       //
//   object.                                                         //
// ----------------------------------------------------------------- //
PUBLIC DataCube *DataCube_copy_noData(const DataCube *source)
{
	// Sanity checks
	check_null(source);
	
	DataCube *self = DataCube_new(source->verbosity);
	
	// Copy header
	self->header = Header_copy(source->header);
	
	// Copy data
	self->data = (char *)memory(MALLOC, source->data_size, source->word_size * sizeof(char));
	//memcpy(self->data, source->data, source->word_size * source->data_size);
	
	// Copy remaining properties
	self->data_size    = source->data_size;
	self->data_type    = source->data_type;
	self->word_size    = source->word_size;
	self->dimension    = source->dimension;
	self->axis_size[0] = source->axis_size[0];
	self->axis_size[1] = source->axis_size[1];
	self->axis_size[2] = source->axis_size[2];
	self->axis_size[3] = source->axis_size[3];
	
	return self;
}




// --------------------------------------------------------- //
// 1D boxcar filter for CUDA                                 //
// --------------------------------------------------------- //
//                                                           //
// Arguments:                                                //
//                                                           //
//   (1)          data - Pointer to data array to be         //
//                       filtered.                           //
//   (2)     data_copy - Pointer to data array to be used    //
//                       for storing a copy of the data dur- //
//                       ring filtering. Its size must be    //
//                       equal to size + 2 * filter_radius.  //
//   (3)          size - Size of input array.                //
//   (4) filter_radius - Radius of boxcar filter.            //
//                                                           //
// Returns:                                                  //
//                                                           //
//   No return value.                                        //
//                                                           //
// Description:                                              //
//                                                           //
//   Applies a boxcar filter to the data array. NOTE that    //
//   this will modify the original data array. NaN values    //
//   will be set to 0 prior to filtering only if replace_nan //
//   is set to true (this is to avoid a check within this    //
//   function for reasons of speed). Values outside of the   //
//   boundaries of the array are assumed to be 0.            //
// --------------------------------------------------------- //

__device__ void filter_boxcar_1d_flt_device(float *data, float *dataCpyArray, const size_t size, const size_t filter_radius, size_t stride)
{	
	// Define filter size
	const size_t filter_size = 2 * filter_radius + 1;
	const float inv_filter_size = 1.0 / filter_size;
	size_t i;

	//float* dataCpyArray = new float[filter_radius + 2];
	for(i = filter_radius + 1; i>0; i--) {
		dataCpyArray[i] = 0;
	}
	int countdataCpyArray = 1;
	dataCpyArray[0]= data[(size - 1)*stride];

	// Apply boxcar filter to last data point
	for(i = filter_radius; i--;) { //desde el anterior en un radio. El mismo y los posteriores no son necesarios
		int iC = size + i - filter_radius - 1;
		if (iC >= 0){
			data[(size - 1)*stride] += data[(iC)*stride];
		}
	}
	data[(size - 1)*stride] *= inv_filter_size;
	
	// Recursively apply boxcar filter to  all previous data points
	for(i = size - 1; i--;) {
		int iC1 = i - filter_radius;
		int iC2 = i + filter_radius + 1;
		float dataCpy1;
		float dataCpy2;

		dataCpyArray[countdataCpyArray] = data[i*stride];
		if (countdataCpyArray >= filter_radius + 1){
			countdataCpyArray = 0;
		}else{
			countdataCpyArray++;
		}

		if (iC1 >= 0) {
			dataCpy1 = data[iC1*stride];
		}else{
			dataCpy1 = 0;
		}
		if (iC2 < size) {
			dataCpy2 = dataCpyArray[countdataCpyArray];
		}else{
			dataCpy2 = 0;
		}
		data[i*stride] = data[(i + 1)*stride] + (dataCpy1 - dataCpy2) * inv_filter_size;
	}
	return;
}

// ----------------------------------------------------------------- //
// Kernel definitions for the gaussian filter                       //
// ----------------------------------------------------------------- //
// Arguments:                                                        //
//                                                                   //
// Return value:                                                     //
//                                                                   //
//   No return value.                                                //
//                                                                   //
// Description:                                                      //
//                                                                   //
// ----------------------------------------------------------------- //
__global__ void Xfilters_flt(char *data, char *dataRadius, size_t radius, size_t sizeX, size_t sizeY, size_t sizeZ){
	int depth = threadIdx.z + blockIdx.z * blockDim.z;
	int row   = threadIdx.y + blockIdx.y * blockDim.y;

	if ((depth < sizeZ) && (row < sizeY)){
		//puntero a la posición inicial de la fila a analizar: plano depth, fila row.
		float *data_i = (float*) data + row * sizeX + depth * sizeX * sizeY;
		
		float *dataRadius_i = (float*) dataRadius + (row + depth* sizeY) * (radius + 2);

		filter_boxcar_1d_flt_device(data_i, dataRadius_i, sizeX, radius, 1);
	}
}
__global__ void Yfilters_flt(char *data, char *dataRadius, size_t radius, size_t sizeX, size_t sizeY, size_t sizeZ){
	int depth = threadIdx.z + blockIdx.z * blockDim.z;
	int col   = threadIdx.x + blockIdx.x * blockDim.x;

	if ((col < sizeX) && (depth < sizeZ)){
		//puntero a la posición inicial de la columna a analizar marcada por threadId = x0:xN
		float *data_i = (float*) data + col + depth * sizeX * sizeY;

		float *dataRadius_i = (float*) dataRadius + (col + depth * sizeX) * (radius + 2);

		filter_boxcar_1d_flt_device(data_i, dataRadius_i, sizeX, radius, sizeX);
	}
}

__global__ void Xfilters_flt_sharedMem(char *data, size_t radius, size_t sizeX, size_t sizeY, size_t sizeZ){
	int depth = threadIdx.z + blockIdx.z * blockDim.z;
	int row   = threadIdx.y + blockIdx.y * blockDim.y;

	if ((depth < sizeZ) && (row < sizeY)){
		//puntero a la posición inicial de la fila a analizar: plano depth, fila row.
		float *data_i = (float*) data + row * sizeX + depth * sizeX * sizeY;
		
		extern __shared__ float smem[];
		float* dataCpyArray = (float*) &smem[(threadIdx.y + threadIdx.z * blockDim.y) * (radius + 2)];

		filter_boxcar_1d_flt_device(data_i, dataCpyArray, sizeX, radius, 1);
	}
}
__global__ void Yfilters_flt_sharedMem(char *data, size_t radius, size_t sizeX, size_t sizeY, size_t sizeZ){
	int depth = threadIdx.z + blockIdx.z * blockDim.z;
	int col   = threadIdx.x + blockIdx.x * blockDim.x;

	if ((col < sizeX) && (depth < sizeZ)){
		//puntero a la posición inicial de la columna a analizar marcada por threadId = x0:xN
		float *data_i = (float*) data + col + depth * sizeX * sizeY;

		extern __shared__ float smem[];
		float* dataCpyArray = (float*) &smem[(threadIdx.x + threadIdx.z * blockDim.x) * (radius + 2)];

		filter_boxcar_1d_flt_device(data_i, dataCpyArray, sizeX, radius, sizeX);
	}
}

// ----------------------------------------------------------------- //
// Apply 2D Gaussian filter to spatial planes                        //
// ----------------------------------------------------------------- //
// Arguments:                                                        //
//                                                                   //
//   (1) self    - Object self-reference.                            //
//   (2) sigma   - Standard deviation of the Gaussian in pixels.     //
//                                                                   //
// Return value:                                                     //
//                                                                   //
//   No return value.                                                //
//                                                                   //
// Description:                                                      //
//                                                                   //
//   Public method for convolving each spatial image plane (x-y) of  //
//   the data cube with a Gaussian function of standard deviation    //
//   sigma. The Gaussian convolution is approximated through a set   //
//   of 1D boxcar filters, which makes the algorithm extremely fast. //
//   Limitations from this approach are that the resulting convolu-  //
//   tion kernel is only an approximation of a Gaussian (although a  //
//   fairly accurate one) and the value of sigma can only be appro-  //
//   ximated (typically within +/- 0.2 sigma) and must be at least   //
//   1.5 pixels.                                                     //
//   The algorithm is NaN-safe by setting all NaN values to 0. Any   //
//   pixel outside of the image boundaries is also assumed to be 0.  //
// ----------------------------------------------------------------- //

PUBLIC void DataCube_gaussian_filter_device(char *cube, const double sigma, size_t sizeX, size_t sizeY, size_t sizeZ, int data_type)
{
	// Set up parameters required for boxcar filter
	size_t n_iter;
	size_t filter_radius;
	optimal_filter_size_dbl(sigma, &filter_radius, &n_iter);

	if(data_type == -32)
	{
		char* dataRadiusX;
		cudaMalloc(&dataRadiusX, sizeY * sizeZ * (filter_radius + 2) * sizeof(float));

		dim3 dimBlockFX(1, 16 , 16);
		int dimGridX = 1;
		int dimGridY = (sizeY + dimBlockFX.y - 1) / dimBlockFX.y;
		int dimGridZ = (sizeZ + dimBlockFX.z - 1) / dimBlockFX.z;
		dim3 dimGridFX(dimGridX, dimGridY, dimGridZ);

		cudaDeviceSynchronize();
		for(size_t i = n_iter; i--;) {
			Xfilters_flt<<<dimGridFX, dimBlockFX>>>(cube, dataRadiusX, filter_radius, sizeX, sizeY, sizeZ);
			cudaDeviceSynchronize();
		}

		cudaFree(dataRadiusX);

		char* dataRadiusY;
		cudaMalloc(&dataRadiusY, sizeX * sizeZ * (filter_radius + 2) * sizeof(float));

		dim3 dimBlockFY(16, 1, 16);
		dimGridX = (sizeX + dimBlockFY.x - 1) / dimBlockFY.x;
		dimGridY = 1;
		dimGridZ = (sizeZ + dimBlockFY.z - 1) / dimBlockFY.z;
		dim3 dimGridFY(dimGridX, dimGridY, dimGridZ);

		for(size_t i = n_iter; i--;) {
			Yfilters_flt<<<dimGridFY, dimBlockFY>>>(cube, dataRadiusY, filter_radius, sizeX, sizeY, sizeZ);
			cudaDeviceSynchronize();
		}

		cudaFree(dataRadiusY);
	}
	else
	{

	}

	return;
}

PUBLIC void DataCube_gaussian_filter_device_sharedMem(char *cube, const double sigma, size_t sizeX, size_t sizeY, size_t sizeZ, int data_type)
{
	// Set up parameters required for boxcar filter
	size_t n_iter;
	size_t filter_radius;
	optimal_filter_size_dbl(sigma, &filter_radius, &n_iter);

	if(data_type == -32)
	{
		dim3 dimBlockFX(1, 16 , 16);
		int dimGridX = 1;
		int dimGridY = (sizeY + dimBlockFX.y - 1) / dimBlockFX.y;
		int dimGridZ = (sizeZ + dimBlockFX.z - 1) / dimBlockFX.z;
		dim3 dimGridFX(dimGridX, dimGridY, dimGridZ);

		cudaDeviceSynchronize();
		for(size_t i = n_iter; i--;) {
			Xfilters_flt_sharedMem<<<dimGridFX, dimBlockFX, 16 * 16 * (filter_radius + 2) * sizeof(float)>>>(cube, filter_radius, sizeX, sizeY, sizeZ);
			cudaDeviceSynchronize();
		}

		dim3 dimBlockFY(16, 1, 16);
		dimGridX = (sizeX + dimBlockFY.x - 1) / dimBlockFY.x;
		dimGridY = 1;
		dimGridZ = (sizeZ + dimBlockFY.z - 1) / dimBlockFY.z;
		dim3 dimGridFY(dimGridX, dimGridY, dimGridZ);

		for(size_t i = n_iter; i--;) {
			Yfilters_flt_sharedMem<<<dimGridFY, dimBlockFY, 16 * 16 * (filter_radius + 2) * sizeof(float)>>>(cube, filter_radius, sizeX, sizeY, sizeZ);
			cudaDeviceSynchronize();
		}
	}
	else
	{

	}

	return;
}

// ----------------------------------------------------------------- //
// Kernel definitions for the boxcar filter                         //
// ----------------------------------------------------------------- //
// Arguments:                                                        //
//                                                                   //
// Return value:                                                     //
//                                                                   //
//   No return value.                                                //
//                                                                   //
// Description:                                                      //
//                                                                   //
// ----------------------------------------------------------------- //
__global__ void Zfilters_flt(char *data, char *dataRadius, size_t radius, size_t sizeX, size_t sizeY, size_t sizeZ){
	int col = threadIdx.x + blockIdx.x * blockDim.x;
	int row = threadIdx.y + blockIdx.y * blockDim.y;

	if ((col < sizeX) && (row < sizeY)){
		//puntero a la posición inicial de la columna a analizar marcada por threadId = x0:xN
		float *data_i = (float*) data + col + row * sizeX;

		float *dataRadius_i = (float*) dataRadius + (col + row * sizeX) * (radius + 2);
		
		filter_boxcar_1d_flt_device(data_i, dataRadius_i, sizeZ, radius, sizeX * sizeY);
	}
}

__global__ void Zfilters_flt_sharedMem(char *data, size_t radius, size_t sizeX, size_t sizeY, size_t sizeZ){
	int col = threadIdx.x + blockIdx.x * blockDim.x;
	int row = threadIdx.y + blockIdx.y * blockDim.y;

	if ((col < sizeX) && (row < sizeY)){
		//puntero a la posición inicial de la columna a analizar marcada por threadId = x0:xN
		float *data_i = (float*) data + col + row * sizeX;

		extern __shared__ float smem[];
		float* dataCpyArray = (float*) &smem[(threadIdx.x + threadIdx.y * blockDim.x) * (radius + 2)];

		filter_boxcar_1d_flt_device(data_i, dataCpyArray, sizeZ, radius, sizeX * sizeY);
	}
}

// ----------------------------------------------------------------- //
// Apply boxcar filter to spectral axis                              //
// ----------------------------------------------------------------- //
// Arguments:                                                        //
//                                                                   //
//   (1) self    - Object self-reference.                            //
//   (2) radius  - Filter radius in channels.                        //
//                                                                   //
// Return value:                                                     //
//                                                                   //
//   No return value.                                                //
//                                                                   //
// Description:                                                      //
//                                                                   //
//   Public method for convolving each spectrum of the data cube     //
//   with a boxcar filter of size 2 * radius + 1. The algorithm is   //
//   NaN-safe by setting all NaN values to 0 prior to filtering. Any //
//   pixel outside of the cube's spectral range is also assumed to   //
//   be 0.                                                           //
// ----------------------------------------------------------------- //

PUBLIC void DataCube_boxcar_filter_device(char *cube, size_t radius, size_t sizeX, size_t sizeY, size_t sizeZ, int data_type)
{
	// Sanity checks
	if(radius < 1) radius = 1;

	if(data_type == -32)
	{
		char* dataRadiusZ;
		cudaMalloc(&dataRadiusZ, sizeX * sizeY * (radius + 2) * sizeof(float));

		dim3 dimBlock(16 , 16);
		int dimGridX = (sizeX + dimBlock.x - 1) / dimBlock.x;
		int dimGridY = (sizeY + dimBlock.y - 1) / dimBlock.y;
		int dimGridZ = 1;
		dim3 dimGrid(dimGridX, dimGridY, dimGridZ);

		cudaDeviceSynchronize();
		Zfilters_flt<<<dimGrid, dimBlock>>>(cube, dataRadiusZ, radius, sizeX, sizeY, sizeZ);
		cudaDeviceSynchronize();

		cudaFree(dataRadiusZ);
	}
	else
	{

	}
	return;
}

PUBLIC void DataCube_boxcar_filter_device_sharedMem(char *cube, size_t radius, size_t sizeX, size_t sizeY, size_t sizeZ, int data_type)
{
	// Sanity checks
	if(radius < 1) radius = 1;

	if(data_type == -32)
	{
		dim3 dimBlock(16 , 16);
		int dimGridX = (sizeX + dimBlock.x - 1) / dimBlock.x;
		int dimGridY = (sizeY + dimBlock.y - 1) / dimBlock.y;
		int dimGridZ = 1;
		dim3 dimGrid(dimGridX, dimGridY, dimGridZ);

		cudaDeviceSynchronize();
		Zfilters_flt_sharedMem<<<dimGrid, dimBlock, 16 * 16 * (radius + 2) * sizeof(float)>>>(cube, radius, sizeX, sizeY, sizeZ);
		cudaDeviceSynchronize();
	}
	else
	{

	}
	return;
}